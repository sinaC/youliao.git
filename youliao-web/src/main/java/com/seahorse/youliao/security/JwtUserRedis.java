package com.seahorse.youliao.security;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @ProjectName: youliao
 * @Package: com.seahorse.youliao.security
 * @ClassName: JwtUserRedis
 * @Description: 用户信息序列化
 * @author:songqiang
 * @Date:2020-11-27 12:39
 **/
public class JwtUserRedis implements Serializable {

    /**
     * 用户名
     */
    private String username;

    /**
     * 权限集合
     */
    private Collection<GrantedAuthorityImpl> authorities;

    /**
     * 登录时间
     */
    private Date loginTime;

    /**
     * 登录ip
     */
    private String loginIp;

    /**
     * 登录地
     */
    private String loginLocation;

    public JwtUserRedis() {
    }


    public JwtUserRedis(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<GrantedAuthorityImpl> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<GrantedAuthorityImpl> authorities) {
        this.authorities = authorities;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getLoginLocation() {
        return loginLocation;
    }

    public void setLoginLocation(String loginLocation) {
        this.loginLocation = loginLocation;
    }
}
